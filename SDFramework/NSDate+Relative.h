//
//  NSDate+Relative.h
//  SDFramework
//
//  Created by Silvio Daminato on 01/07/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Relative)

- (NSString *)stringWithElapsedTimeSinceNow;
- (NSString *)smartDateString;
- (NSString *)smartDateStringWithTime;

@end
