//
//  ADNetworkingManager.m
//  Adaptica
//
//  Created by Silvio Daminato on 16/09/14.
//  Copyright (c) 2014 Silvio Daminato. All rights reserved.
//

#import "SDNetworkingManager.h"
#import <AFNetworking.h>
#import <Reachability.h>
#import <AFNetworkActivityIndicatorManager.h>

#define kAuthorizationTokenKey @"kAuthorizationTokenKey"

#define kHostNameInfoPlistKey @"SDHostName"
#define kApiPrefixInfoPlistKey @"SDApiPrefix"
#define kHttpsInfoPlistKey @"SDUseHttps"
#define kAuthorizationTokenHeaderKey @"SDAuthorizationTokenHeaderKey"
#define kRequestBodyXML @"SDXMLRequestBody"
#define kResponseXML @"SDXMLResponse"

@interface SDNetworkingManager ()

@property (nonatomic, strong) Reachability * reach;

@property (nonatomic, strong) NSString * protocol;
@property (nonatomic, strong) NSString * hostName;
@property (nonatomic, strong) NSString * serverAddress;
@property (nonatomic, strong) NSString * authorizationTokenHeaderKey;

@end

@implementation SDNetworkingManager

- (instancetype)init
{
	self = [super init];
	if(self) {
		NSDictionary * infoPlist = [[NSBundle mainBundle] infoDictionary];
		self.hostName = infoPlist[kHostNameInfoPlistKey];
		if (!self.hostName) {
			NSLogError(@"[ERROR] Please insert the key \"SDHostName\" with the host name in your Info.plist file");
			return nil;
		}
		NSString * apiPrefix = infoPlist[kApiPrefixInfoPlistKey];
		if (!apiPrefix) {
			apiPrefix = @"";
		}
		self.protocol = [infoPlist[kHttpsInfoPlistKey] boolValue] ? @"https" : @"http";
		self.serverAddress = [NSString stringWithFormat:@"%@://%@%@", self.protocol, self.hostName, apiPrefix];
		
		if (infoPlist[kAuthorizationTokenHeaderKey]) {
			self.authorizationTokenHeaderKey = infoPlist[kAuthorizationTokenHeaderKey];
		} else {
			self.authorizationTokenHeaderKey = @"Authorization-Token";
		}
		
		self.httpHeaders = nil;
		
		[AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
		_reach = [Reachability reachabilityWithHostname:self.hostName];
		[_reach startNotifier];
		
		_authorizationToken = [[NSUserDefaults standardUserDefaults] objectForKey:kAuthorizationTokenKey];
	}
	return self;
}

#pragma mark - Public methods

+ (instancetype)sharedManager
{
	static id INSTANCE = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		INSTANCE = [[self alloc] init];
	});
	return INSTANCE;
}

- (AFHTTPSessionManager *)manager
{
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	
	NSDictionary * infoPlist = [[NSBundle mainBundle] infoDictionary];

	AFHTTPRequestSerializer * serializer;
	
	if ([infoPlist[kRequestBodyXML] boolValue]) {
		serializer = [[AFHTTPRequestSerializer alloc] init];
		[serializer setQueryStringSerializationWithBlock:^NSString * _Nonnull(NSURLRequest * _Nonnull request, id  _Nonnull parameters, NSError * _Nullable __autoreleasing * _Nullable error) {
			NSString * requestBody = [self convertDictionarytoXML:parameters withStartElement:@"Request"];
//			NSLog(@"%@", requestBody);
			return requestBody;
		}];
	} else {
		serializer = [AFJSONRequestSerializer serializer];
	}
	[serializer setValue:self.authorizationToken forHTTPHeaderField:self.authorizationTokenHeaderKey];
	for (NSString * key in self.httpHeaders) {
		[serializer setValue:self.httpHeaders[key] forHTTPHeaderField:key];
	}
	manager.requestSerializer = serializer;

	if ([infoPlist[kResponseXML] boolValue]) {
		AFXMLParserResponseSerializer * xmlResponseSerializer = [AFXMLParserResponseSerializer serializer];
		NSMutableSet *xmlAcceptableContentTypes = [NSMutableSet setWithSet:xmlResponseSerializer.acceptableContentTypes];
		[xmlAcceptableContentTypes addObject:@"text/xml"];
		[xmlAcceptableContentTypes addObject:@"application/rss+xml"];
		xmlResponseSerializer.acceptableContentTypes = xmlAcceptableContentTypes;
		manager.responseSerializer = xmlResponseSerializer;
	} else {
		AFJSONResponseSerializer * jsonResponseSerializer = [AFJSONResponseSerializer serializer];
		NSMutableSet *jsonAcceptableContentTypes = [NSMutableSet setWithSet:jsonResponseSerializer.acceptableContentTypes];
		[jsonAcceptableContentTypes addObject:@"application/hal+json"];
		[jsonAcceptableContentTypes addObject:@"application/problem+json"];
		
		jsonResponseSerializer.acceptableContentTypes = jsonAcceptableContentTypes;
		manager.responseSerializer = jsonResponseSerializer;
	}
	return manager;
}

- (void)GET:(NSString *)endPoint
 parameters:(id)parameters
	success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure
{
	[self GET:endPoint host:self.serverAddress parameters:parameters success:success failure:failure];
}

- (void)GET:(NSString *)endPoint
	   host:(NSString *)host
 parameters:(id)parameters
	success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure
{
	NSString * url = [host stringByAppendingString:endPoint];
	NSLogNetwork(@"[GET] %@ params: %@", url, parameters);
	[self.manager GET:url parameters:parameters progress:nil success:success failure:failure];
}

- (void)GET:(NSString *)endPoint
	   host:(NSString *)host
 parameters:(id)parameters
	headers:(NSDictionary *)headers
	success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure
{
	NSString * url = [host stringByAppendingString:endPoint];
	NSLogNetwork(@"[GET] %@ params: %@", url, parameters);
	
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	[manager.requestSerializer setValue:self.authorizationToken forHTTPHeaderField:self.authorizationTokenHeaderKey];
	for (NSString * key in headers) {
		[manager.requestSerializer setValue:headers[key] forHTTPHeaderField:key];
	}
	AFJSONResponseSerializer * jsonResponseSerializer = [AFJSONResponseSerializer serializer];
	NSMutableSet *jsonAcceptableContentTypes = [NSMutableSet setWithSet:jsonResponseSerializer.acceptableContentTypes];
	[jsonAcceptableContentTypes addObject:@"application/hal+json"];
	[jsonAcceptableContentTypes addObject:@"application/problem+json"];
	jsonResponseSerializer.acceptableContentTypes = jsonAcceptableContentTypes;
	manager.responseSerializer = jsonResponseSerializer;
	
	[manager GET:url parameters:parameters progress:nil success:success failure:failure];
}

- (void)postToUrl:(NSString *)url
	   parameters:(id)parameters
		  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
		  failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure
{
	NSLogNetwork(@"[POST] %@ params: %@", url, parameters);
	[self.manager POST:url parameters:parameters progress:nil success:success failure:failure];
}

- (void)POST:(NSString *)endPoint
  parameters:(id)parameters
	 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	 failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure
{
	NSString * url = [self.serverAddress stringByAppendingString:endPoint];
	NSLogNetwork(@"[POST] %@ params: %@", url, parameters);
	[self.manager POST:url parameters:parameters progress:nil success:success failure:failure];
}

- (void)PUT:(NSString *)endPoint
 parameters:(id)parameters
	success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure
{
	NSString * url = [self.serverAddress stringByAppendingString:endPoint];
	NSLogNetwork(@"[PUT] %@ params: %@", url, parameters);
	[self.manager PUT:url parameters:parameters success:success failure:failure];
}

- (void)POST:(NSString *)endPoint
  parameters:(id)parameters
constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
	 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	 failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure
{
	NSString * url = [self.serverAddress stringByAppendingString:endPoint];
	NSLogNetwork(@"[Multipart POST] %@ params: %@", url, parameters);
	[self.manager POST:url parameters:parameters constructingBodyWithBlock:block progress:nil success:success failure:failure];
}

- (void)DELETE:(NSString *)endPoint
	parameters:(id)parameters
	   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	   failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure
{
	NSString * url = [self.serverAddress stringByAppendingString:endPoint];
	NSLogNetwork(@"[DELETE] %@ params: %@", url, parameters);
	[self.manager DELETE:url parameters:parameters success:success failure:failure];
}

- (void)PATCH:(NSString *)endPoint
   parameters:(id)parameters
	  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	  failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure
{
	NSString * url = [self.serverAddress stringByAppendingString:endPoint];
	NSLogNetwork(@"[PATCH] %@ params: %@", url, parameters);
	[self.manager PATCH:url parameters:parameters success:success failure:failure];
}

#pragma mark - Private methods

- (NSString*)convertDictionarytoXML:(NSDictionary*)dictionary withStartElement:(NSString*)startElement{
	NSMutableString *xml = [[NSMutableString alloc] initWithString:@""];
	[xml appendString:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"];
	[xml appendString:[NSString stringWithFormat:@"<%@>",startElement]];
	[self convertNode:dictionary withString:xml andTag:nil];
	[xml appendString:[NSString stringWithFormat:@"</%@>",startElement]];
	NSString *finalXML=[xml stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
//	NSLog(@"%@",xml);
	return finalXML;
}

- (void)convertNode:(id)node withString:(NSMutableString *)xml andTag:(NSString *)tag{
	if ([node isKindOfClass:[NSDictionary class]] && !tag) {
		NSArray *keys = [node allKeys];
		for (NSString *key in keys) {
			[self convertNode:[node objectForKey:key] withString:xml andTag:key];
		}
	} else if ([node isKindOfClass:[NSArray class]]) {
		for (id value in node) {
			[self convertNode:value withString:xml andTag:tag];
		}
	} else {
		[xml appendString:[NSString stringWithFormat:@"<%@>", tag]];
		if ([node isKindOfClass:[NSString class]]) {
			[xml appendString:node];
		} else if ([node isKindOfClass:[NSNumber class]]) {
			[xml appendString:[node description]];
		} else if ([node isKindOfClass:[NSDictionary class]]) {
			[self convertNode:node withString:xml andTag:nil];
		}
		[xml appendString:[NSString stringWithFormat:@"</%@>", tag]];
	}
}

#pragma mark - Properties

- (NSString *)protocol
{
	return _protocol;
}

- (NSString *)host
{
	return self.hostName;
}

- (NSString *)baseServerUrl
{
	return self.serverAddress;
}

- (void)setAuthorizationToken:(NSString *)authorizationToken
{
	_authorizationToken = authorizationToken;
	[[NSUserDefaults standardUserDefaults] setValue:authorizationToken forKey:kAuthorizationTokenKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setHttpHeaders:(NSDictionary *)httpHeaders
{
	NSMutableDictionary * headers;
	if (httpHeaders) {
		headers = [httpHeaders mutableCopy];
	} else {
		headers = [NSMutableDictionary dictionary];
	}
	
	[headers setObject:@"application/json" forKey:@"Accept"];
	[headers setObject:@"application/json" forKey:@"Content-Type"];
	
	_httpHeaders = headers;
}

- (BOOL)networkReachable
{
	return self.reach.isReachable;
}

- (void)setReachableBlock:(void (^)(Reachability *))reachableBlock
{
	_reachableBlock = reachableBlock;
	self.reach.reachableBlock = reachableBlock;
}

- (void)setUnreachableBlock:(void (^)(Reachability *))unreachableBlock
{
	_unreachableBlock = unreachableBlock;
	self.reach.unreachableBlock = unreachableBlock;
}

- (NSError *)errorWithMessage:(NSString *)message
{
	NSDictionary *userInfo = @{
							   NSLocalizedDescriptionKey: NSLocalizedString(message, nil)
							   };
	NSError *error = [NSError errorWithDomain:@"SDFramework" code:1 userInfo:userInfo];
	return error;
}

@end
