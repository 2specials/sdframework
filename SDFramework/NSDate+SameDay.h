//
//  NSDate+SameDay.h
//  SDFramework
//
//  Created by Silvio Daminato on 26/04/18.
//  Copyright © 2018 2Specials SRL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (SameDay)

- (BOOL)sameDayAsDate:(NSDate *)date;

@end
