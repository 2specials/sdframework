//
//  SDDismissSegue.m
//  SDFramework
//
//  Created by Silvio Daminato on 13/04/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import "SDDismissSegue.h"

@implementation SDDismissSegue

- (void)perform
{
	[self.sourceViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
