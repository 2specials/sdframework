//
//  SDFlipSegue.m
//  SDFramework
//
//  Created by Silvio Daminato on 12/04/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import "SDFlipSegue.h"

@implementation SDFlipSegue

- (void)perform
{
	UINavigationController * navigationController = self.sourceViewController.navigationController;
	NSMutableArray * viewControllers = navigationController.viewControllers.mutableCopy;
	[viewControllers removeLastObject];
	[viewControllers addObject:self.destinationViewController];
	[navigationController setViewControllers:viewControllers];
	
	[UIView transitionFromView:self.sourceViewController.view toView:self.destinationViewController.view duration:0.3 options:UIViewAnimationOptionTransitionFlipFromRight completion:^(BOOL finished) {
		
	}];
}

@end
