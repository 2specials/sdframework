//
//  UIView+Screenshot.h
//  Flics
//
//  Created by Silvio Daminato on 17/07/13.
//  Copyright (c) 2013 Corso12 SRL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Screenshot)

- (UIImage *)screenshot;
- (UIImage *)screenshotWithScale:(CGFloat)scale;

@end
