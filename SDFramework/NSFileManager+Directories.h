//
//  NSFileManager+Directories.h
//  Kenwood Club
//
//  Created by Silvio Daminato on 08/10/15.
//  Copyright © 2015 2Specials srl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (Directories)

@property (nonatomic, readonly) NSString * cacheDirectory;
@property (nonatomic, readonly) NSString * documentsDirectory;

@end
