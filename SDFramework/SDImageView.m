//
//  SDImageView.m
//  SDFramework
//
//  Created by Silvio Daminato on 18/01/18.
//  Copyright © 2018 2Specials SRL. All rights reserved.
//

#import "SDImageView.h"

@implementation SDImageView

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.layer.masksToBounds = YES;
	self.layer.cornerRadius = self.cornerRadius;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	self.layer.cornerRadius = self.cornerRadius;
	
	self.layer.borderWidth = self.borderWidth;
	self.layer.borderColor = self.borderColor ? self.borderColor.CGColor : self.tintColor.CGColor;
}

@end
