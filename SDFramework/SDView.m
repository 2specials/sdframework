//
//  SDView.m
//  SDFramework
//
//  Created by Silvio Daminato on 01/02/17.
//  Copyright © 2017 2Specials SRL. All rights reserved.
//

#import "SDView.h"

@implementation SDView

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.layer.borderWidth = self.borderWidth;
	self.layer.borderColor = self.borderColor ? self.borderColor.CGColor : self.tintColor.CGColor;
	self.layer.cornerRadius = self.cornerRadius;
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
	_borderWidth = borderWidth;
	self.layer.borderWidth = borderWidth;
}

- (void)setBorderColor:(UIColor *)borderColor
{
	_borderColor = borderColor;
	self.layer.borderColor = borderColor.CGColor;
}

- (void)setCornerRadius:(CGFloat)cornerRadius
{
	_cornerRadius = cornerRadius;
	self.layer.cornerRadius = cornerRadius;
}

@end
