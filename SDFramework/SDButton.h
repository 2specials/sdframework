//
//  SDButton.h
//  SDFramework
//
//  Created by Silvio Daminato on 01/02/17.
//  Copyright © 2017 2Specials SRL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDButton : UIButton

@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic, strong) IBInspectable UIColor * borderColor;
@property (nonatomic) IBInspectable CGFloat cornerRadius;

@end
