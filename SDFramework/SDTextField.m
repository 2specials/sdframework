//
//  SDTextField.m
//  SDFramework
//
//  Created by Silvio Daminato on 07/04/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import "SDTextField.h"

@interface SDTextField () <UITextFieldDelegate>

@end

@implementation SDTextField

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	if (self.placeholderColor) {
		self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName : self.placeholderColor , NSFontAttributeName : self.font}];
	}
	
	if (self.doneButtonAccessoryInput) {
		UIToolbar * toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 0, 44)];
		toolbar.barStyle = UIBarStyleBlackTranslucent;
		toolbar.barTintColor = self.toolbarTintColor ? self.toolbarTintColor : [UIColor whiteColor];
		UIBarButtonItem * spaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
		UIBarButtonItem * doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed:)];
		if (self.doneButtonTintColor) {
			doneBarButtonItem.tintColor = self.doneButtonTintColor;
		} else {
			doneBarButtonItem.tintColor = self.tintColor;
		}
		toolbar.items = @[spaceBarButtonItem, doneBarButtonItem];
		self.inputAccessoryView = toolbar;
	}
	
	self.layer.borderWidth = self.borderWidth;
	self.layer.borderColor = self.borderColor ? self.borderColor.CGColor : self.tintColor.CGColor;
	self.layer.cornerRadius = self.cornerRadius;
	self.layer.masksToBounds = YES;
}

- (void)doneButtonPressed:(id)sender
{
	[self resignFirstResponder];
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
	_borderWidth = borderWidth;
	self.layer.borderWidth = borderWidth;
}

- (void)setBorderColor:(UIColor *)borderColor
{
	_borderColor = borderColor;
	self.layer.borderColor = borderColor.CGColor;
}

- (void)setCornerRadius:(CGFloat)cornerRadius
{
	_cornerRadius = cornerRadius;
	self.layer.cornerRadius = cornerRadius;
}

@end
