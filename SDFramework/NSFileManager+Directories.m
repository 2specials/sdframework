//
//  NSFileManager+Directories.m
//  Kenwood Club
//
//  Created by Silvio Daminato on 08/10/15.
//  Copyright © 2015 2Specials srl. All rights reserved.
//

#import "NSFileManager+Directories.h"

@implementation NSFileManager (Directories)

- (NSString *)cacheDirectory
{
	return NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject;
}

- (NSString *)documentsDirectory
{
	return NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
}

@end
