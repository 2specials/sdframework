//
//  SDCircleView.m
//  SDFramework
//
//  Created by Silvio Daminato on 12/04/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import "SDCircleView.h"

@implementation SDCircleView

- (void)awakeFromNib
{
	[super awakeFromNib];
	self.cornerRadius = self.frame.size.width > self.frame.size.height ? self.frame.size.height / 2 : self.frame.size.width / 2;
}

- (void)setFrame:(CGRect)frame
{
	[super setFrame:frame];
	self.cornerRadius = frame.size.width > frame.size.height ? frame.size.height / 2 : frame.size.width / 2;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	self.cornerRadius = self.frame.size.width > self.frame.size.height ? self.frame.size.height / 2 : self.frame.size.width / 2;
}

@end
