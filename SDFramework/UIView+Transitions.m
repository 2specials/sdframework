//
//  UIView+Transitions.m
//  SDFramework
//
//  Created by Silvio Daminato on 18/01/17.
//  Copyright © 2017 2Specials SRL. All rights reserved.
//

#import "UIView+Transitions.h"

@implementation UIView (Transitions)

- (void)addTransitionWithType:(NSString *)type subtype:(NSString *)subtype duration:(NSTimeInterval)duration
{
	CATransition * transition = [[CATransition alloc] init];
	transition.type = type;
	transition.subtype = subtype;
	transition.duration = duration;
	[self.layer addAnimation:transition forKey:kCATransition];
}

@end
