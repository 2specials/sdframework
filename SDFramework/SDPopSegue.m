//
//  SDPopSegue.m
//  Kenwood Club
//
//  Created by Silvio Daminato on 10/12/15.
//  Copyright © 2015 2Specials srl. All rights reserved.
//

#import "SDPopSegue.h"

@implementation SDPopSegue

- (void)perform
{
	UINavigationController * navigationController = self.sourceViewController.navigationController;
	if ([navigationController.viewControllers containsObject:self.destinationViewController]) {
		[navigationController popToViewController:self.destinationViewController animated:YES];
	} else {
		[navigationController popViewControllerAnimated:YES];
	}
}

@end
