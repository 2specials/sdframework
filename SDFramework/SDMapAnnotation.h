//
//  SDMapAnnotation.h
//  SDFramework
//
//  Created by Silvio Daminato on 26/08/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface SDMapAnnotation : NSObject <MKAnnotation>

- (id)initWithLocation:(CLLocationCoordinate2D)coord;

@end
