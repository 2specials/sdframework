//
//  FLBorderedButton.h
//  Floome
//
//  Created by Silvio Daminato on 05/06/15.
//  Copyright (c) 2015 Toodev. All rights reserved.
//

#import "SDButton.h"

//IB_DESIGNABLE

__attribute__ ((deprecated))
@interface SDBorderedButton : SDButton

@end
