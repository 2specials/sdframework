//
//  UIImage+Blur.m
//  iSetlist
//
//  Created by Silvio Daminato on 20/08/14.
//  Copyright (c) 2014 Silvio Daminato. All rights reserved.
//

#import "UIImage+Blur.h"

@implementation UIImage (Blur)

- (UIImage *)blurredmage
{
	CIContext * context = [CIContext contextWithOptions:nil];
    CIImage * inputImage = [CIImage imageWithCGImage:self.CGImage];
	
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:20.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
	
	//	filter = [CIFilter filterWithName:@"CIColorControls"];
	//	[filter setValue:result forKey:kCIInputImageKey];
	//	[filter setValue:@0.6f forKey:@"inputSaturation"];
	//	[filter setValue:@-0.3f forKey:@"inputBrightness"];
	//	[filter setValue:@0.5f forKey:@"inputContrast"];
	//	result = [filter valueForKey:kCIOutputImageKey];
	
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    UIImage *returnImage = [UIImage imageWithCGImage:cgImage scale:self.scale orientation:self.imageOrientation];
    CGImageRelease(cgImage);
	
    return returnImage;
}

@end
