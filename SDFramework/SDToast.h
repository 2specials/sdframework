//
//  SDToast.h
//  Adaptica
//
//  Created by Silvio Daminato on 03/09/15.
//  Copyright (c) 2015 Silvio Daminato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDToast : UIView

+ (void)makeToast:(NSString *)text;
+ (void)makeToast:(NSString *)text duration:(NSTimeInterval)duration;

@end
