//
//  NSData+HexString.h
//  FreeMS
//
//  Created by Marco on 11/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSData (HexString)

@property (nonatomic, readonly) NSString * hexStringRepresentation;

@end
