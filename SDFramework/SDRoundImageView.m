//
//  SDRoundImageView.m
//  BitMe
//
//  Created by Silvio Daminato on 10/08/15.
//  Copyright (c) 2015 Silvio Daminato. All rights reserved.
//

#import "SDRoundImageView.h"

@implementation SDRoundImageView

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	self.layer.cornerRadius = self.frame.size.width / 2;
	self.layer.masksToBounds = YES;
	
	self.layer.borderWidth = self.borderWidth;
	self.layer.borderColor = self.borderColor ? self.borderColor.CGColor : self.tintColor.CGColor;
}

@end
