//
//  SDRoundButton.m
//  SDFramework
//
//  Created by Silvio Daminato on 18/10/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import "SDRoundButton.h"

@implementation SDRoundButton

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	self.cornerRadius = self.frame.size.height / 2;
}

@end
