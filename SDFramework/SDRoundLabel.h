//
//  SDRoundLabel.h
//  Kenwood Club
//
//  Created by Silvio Daminato on 02/11/15.
//  Copyright © 2015 2Specials srl. All rights reserved.
//

#import "SDLabel.h"

//IB_DESIGNABLE

__attribute__ ((deprecated))
@interface SDRoundLabel : SDLabel

@end
