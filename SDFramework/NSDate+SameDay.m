//
//  NSDate+SameDay.m
//  SDFramework
//
//  Created by Silvio Daminato on 26/04/18.
//  Copyright © 2018 2Specials SRL. All rights reserved.
//

#import "NSDate+SameDay.h"

@implementation NSDate (SameDay)

- (BOOL)sameDayAsDate:(NSDate *)date
{
	NSCalendar* calendar = [NSCalendar currentCalendar];
	NSDateComponents * selfComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:self];
	NSDateComponents * dateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
	
	return  selfComponents.year == dateComponents.year && selfComponents.month == dateComponents.month && selfComponents.day == dateComponents.day;
}

@end
