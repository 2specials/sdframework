 //
//  SDInfiniteScrollFooterView.h
//  Kenwood Club
//
//  Created by Silvio Daminato on 14/10/15.
//  Copyright © 2015 2Specials srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDInfiniteScrollFooterView : UIView

@property (nonatomic, strong) UIActivityIndicatorView * activityIndicator;

@end
