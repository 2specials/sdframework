//
//  SDMapAnnotation.m
//  SDFramework
//
//  Created by Silvio Daminato on 26/08/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import "SDMapAnnotation.h"

@implementation SDMapAnnotation {
	CLLocationCoordinate2D _coordinate;
}

@synthesize coordinate = _coordinate;

- (id)initWithLocation:(CLLocationCoordinate2D)coord {
	self = [super init];
	if (self) {
		_coordinate = coord;
	}
	return self;
}

- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate
{
	_coordinate = newCoordinate;
}

@end
