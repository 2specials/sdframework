//
//  NSDate+Midnight.h
//  SDFramework
//
//  Created by Silvio Daminato on 22/05/17.
//  Copyright © 2017 2Specials SRL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Midnight)

@property (nonatomic, readonly) NSDate * midnight;

@end
