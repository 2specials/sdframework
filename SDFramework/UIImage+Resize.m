//
//  UIImage+Resize.m
//  Corso12
//
//

#import "UIImage+Resize.h"
#import <CoreImage/CoreImage.h>
#import <QuartzCore/QuartzCore.h>

@implementation UIImage (Resize)

// Restituisce un'immagine ottenuta ridimensionando il ricevente alle dimensioni specificate, senza distorcere o tagliare l'immagine
- (UIImage *)scaleToSize: (CGSize)size {
	return [self scaleToSize: size scale: 0];
}

// Restituisce un'immagine ottenuta ridimensionando il ricevente alle dimensioni e scale factor specificati, senza 
// distorcere o tagliare l'immagine
// Passando 0 come scale factor, viene restituita un'immagine avente lo stesso scale factor del ricevente
- (UIImage *)scaleToSize: (CGSize)size scale: (CGFloat)scale {
	CGContextRef	context;
	CGFloat			aspectRatio;
	CGFloat			resizedAspectRatio;
	CGFloat			scaleFactor;
	CGRect			resizedRect;
	//UIColor			*backgroundColor;
	UIImage			*scaledImage;
	
	// Determina il rettangolo in cui disegnare l'immagine senza distorcela e senza tagliarla
	aspectRatio = self.size.width / self.size.height;
	resizedAspectRatio = size.width / size.height;
	
	if(aspectRatio > resizedAspectRatio) {
		resizedRect.size.width = size.width;
		resizedRect.size.height = size.width / aspectRatio;
	} else {
		resizedRect.size.height = size.height;
		resizedRect.size.width = size.height * aspectRatio;
	}
	
	resizedRect.origin.x = (size.width - resizedRect.size.width) / 2;
	resizedRect.origin.y = (size.height - resizedRect.size.height) / 2;
	resizedRect = CGRectIntegral(resizedRect);
	
	// Disegna l'immagine
	scaleFactor = scale ? scale : self.scale;
	UIGraphicsBeginImageContextWithOptions(size, NO, scaleFactor);
	
	context = UIGraphicsGetCurrentContext();
	//backgroundColor = [UIColor clearColor];
	
	CGContextSetInterpolationQuality(context, kCGInterpolationMedium);
	
    CGContextSetBlendMode(context, kCGBlendModeClear);
	//	CGContextFillRect(context, CGRectMake(0, 0, size.width, size.height));
	
	[self drawInRect: resizedRect];
	scaledImage = UIGraphicsGetImageFromCurrentImageContext();
	
	UIGraphicsEndImageContext();
	
	return scaledImage;
}

// Restituisce un'immagine ottenuta ruotando l'immagine ricevente della quantità specificata
- (UIImage *)rotateImage: (CGFloat)radians {
	CGAffineTransform	transform;
	CGContextRef		contextRef;
	CGRect				rect;
	CGRect				rotatedRect;
	UIImage				*image;
	
	rect = CGRectMake(0, 0, self.size.width, self.size.height);
	transform = CGAffineTransformMakeRotation(radians);
	rotatedRect = CGRectApplyAffineTransform(rect, transform);
	
	UIGraphicsBeginImageContext(rotatedRect.size);
	
	contextRef = UIGraphicsGetCurrentContext();
	
	CGContextTranslateCTM(contextRef, rotatedRect.size.width / 2, rotatedRect.size.height / 2);
	CGContextRotateCTM(contextRef, radians);
	CGContextScaleCTM(contextRef, 1.0, -1.0);
	
	CGContextDrawImage(contextRef, CGRectMake(-self.size.width / 2, -self.size.height / 2, self.size.width, self.size.height), self.CGImage);
	
	image = UIGraphicsGetImageFromCurrentImageContext();
	
	UIGraphicsEndImageContext();
	
	return image;
}

// Restituisce un'immagine quadrata con il centro dell'immagine
- (UIImage *)centralSquareImage
{
	CGFloat squareSize = self.size.width < self.size.height ? self.size.width : self.size.height;
    double x = (self.size.width - squareSize) / 2.0;
    double y = (self.size.height - squareSize) / 2.0;
	
    CGRect cropRect = CGRectMake(x, y, squareSize, squareSize);
    return [self croppedImageInRect:cropRect];
}

- (UIImage *)croppedImageInRect: (CGRect)rect
{
//	VLog(NSStringFromCGRect(rect))
	CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, rect);
	
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
	
    return cropped;
}

@end
