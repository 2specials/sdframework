//
//  NSString+HTMLTags.m
//  Kenwood Club
//
//  Created by Silvio Daminato on 08/10/15.
//  Copyright © 2015 2Specials srl. All rights reserved.
//

#import "NSString+HTMLTags.h"

@implementation NSString (HTMLTags)

- (NSString *)stringByStrippingHTML
{
	NSRange r;
	NSString * s = [self copy];
	while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
		s = [s stringByReplacingCharactersInRange:r withString:@""];
	return [s stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end
