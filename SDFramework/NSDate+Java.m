//
//  NSDate+Java.m
//  SDFramework
//
//  Created by Silvio Daminato on 10/11/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import "NSDate+Java.h"

@implementation NSDate (Java)

+ (NSDate *)dateFromJavaDate:(long)javaDate
{
	return [NSDate dateWithTimeIntervalSince1970:javaDate / 1000];
}

- (long)javaDate
{
	return self.timeIntervalSince1970 * 1000;
}

@end
