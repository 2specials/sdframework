//
//  SDBorderedView.h
//  Kenwood Club
//
//  Created by Silvio Daminato on 08/10/15.
//  Copyright © 2015 2Specials srl. All rights reserved.
//

#import "SDView.h"

//IB_DESIGNABLE

__attribute__ ((deprecated))
@interface SDBorderedView : SDView

@end
