//
//  NSDate+Java.h
//  SDFramework
//
//  Created by Silvio Daminato on 10/11/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Java)

+ (NSDate *)dateFromJavaDate:(long)javaDate;
@property (nonatomic, readonly) long javaDate;

@end
