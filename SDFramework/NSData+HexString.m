//
//  NSData+HexString.m
//  FreeMS
//
//  Created by Marco on 11/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "NSData+HexString.h"

@implementation NSData (HexString)

// Restituisce una stringa contenente la rappresentazione esadecimale dei dati binari contenuti nell'oggetto riceventi
- (NSString *)hexStringRepresentation {
	NSMutableString	*string;
	unsigned char	*dataBytes;
	
	string = [NSMutableString stringWithCapacity: self.length * 2];
	dataBytes = (unsigned char *)[self bytes];
	for(NSUInteger i = 0; i < self.length; i++)
		[string appendFormat: @"%02x", dataBytes[i]];
	
	return string;
}

@end
