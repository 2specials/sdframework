//
//  UIDevice+DeviceName.h
//  SDFramework
//
//  Created by Silvio Daminato on 27/09/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (DeviceName)

@property (nonatomic, readonly) NSString * deviceName;

@end
