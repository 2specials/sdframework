//
//  ADNetworkingManager.h
//  Adaptica
//
//  Created by Silvio Daminato on 16/09/14.
//  Copyright (c) 2014 Silvio Daminato. All rights reserved.
//

#import <Foundation/Foundation.h>

//
// Info.plist keys:
//
// "SDHostName"
// "SDApiPrefix"
// "SDUseHttps"
// "SDAuthorizationTokenHeaderKey"
// "SDXMLRequestBody" - optional
// "SDXMLResponse" - optional
//

@class AFMultipartFormData, AFHTTPSessionManager, Reachability;
@protocol AFMultipartFormData;

@interface SDNetworkingManager : NSObject

+ (instancetype)sharedManager;

- (void)GET:(NSString *)endPoint
 parameters:(id)parameters
	success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure;

- (void)POST:(NSString *)endPoint
  parameters:(id)parameters
	 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	 failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure;

- (void)PUT:(NSString *)endPoint
 parameters:(id)parameters
	success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure;

- (void)GET:(NSString *)endPoint
	   host:(NSString *)host
 parameters:(id)parameters
	success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure;

- (void)GET:(NSString *)endPoint
	   host:(NSString *)host
 parameters:(id)parameters
	headers:(NSDictionary *)headers
	success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure;

- (void)postToUrl:(NSString *)url
	   parameters:(id)parameters
		  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
		  failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure;

- (void)POST:(NSString *)endPoint
  parameters:(id)parameters
constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
	 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	 failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure;

- (void)DELETE:(NSString *)endPoint
	parameters:(id)parameters
	   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	   failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure;

- (void)PATCH:(NSString *)endPoint
   parameters:(id)parameters
	  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
	  failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure;

@property (nonatomic, strong) NSString * authorizationToken;
@property (nonatomic, readonly) NSString * protocol;

// backend host
@property (nonatomic, readonly) NSString * host;
// Base server url format: <protocol>://<host><api prefix>
@property (nonatomic, readonly) NSString * baseServerUrl;

@property (nonatomic, readonly) AFHTTPSessionManager * manager;

@property (nonatomic, readonly) BOOL networkReachable;
@property (nonatomic, strong) void (^reachableBlock) (Reachability * reach);
@property (nonatomic, strong) void (^unreachableBlock) (Reachability * reach);

@property (nonatomic, strong) NSDictionary * httpHeaders;

- (NSError *)errorWithMessage:(NSString *)message;

@end
