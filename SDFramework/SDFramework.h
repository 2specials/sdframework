//
//  SDFramework.h
//  SDFramework
//
//  Created by Silvio Daminato on 07/04/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

// Categories

#import "NSData+HexString.h"
#import "NSFileManager+Directories.h"
#import "NSString+HTMLTags.h"
#import "UIImage+Blur.h"
#import "UIImage+Resize.h"
#import "UIView+Screenshot.h"
#import "UIView+Transitions.h"
#import "NSDate+Java.h"
#import "NSDate+Relative.h"
#import "NSDate+Midnight.h"
#import "NSDate+SameDay.h"
#import "UIDevice+DeviceName.h"

// UIView subclasses

#import "SDButton.h"
#import "SDView.h"
#import "SDLabel.h"
#import "SDRoundImageView.h"
#import "SDImageView.h"
#import "SDInfiniteScrollFooterView.h"
#import "SDTextField.h"
#import "SDTextView.h"
#import "SDToast.h"
#import "SDMapAnnotation.h"

// Segues

#import "SDPopSegue.h"
#import "SDDismissSegue.h"
#import "SDFlipSegue.h"
#import "SDNetworkingManager.h"

// Deprecated classes

#import "SDBorderedButton.h"
#import "SDRoundButton.h"
#import "SDBorderedView.h"
#import "SDCircleView.h"
#import "SDRoundLabel.h"
