//
//  SDToast.m
//  Adaptica
//
//  Created by Silvio Daminato on 03/09/15.
//  Copyright (c) 2015 Silvio Daminato. All rights reserved.
//

#import "SDToast.h"


@interface SDToast ()

@property (nonatomic, strong) UILabel * label;

@end

@implementation SDToast

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		self.layer.cornerRadius = 7;
		self.backgroundColor = [UIColor colorWithWhite:0 alpha:.7];
		
		CGRect labelFrame = frame;
		labelFrame.origin = CGPointMake(20, 0);
		labelFrame.size.width -= 40;
		self.label = [[UILabel alloc] initWithFrame:labelFrame];
		self.label.font = [UIFont systemFontOfSize:18];
		self.label.textColor = [UIColor whiteColor];
		self.label.numberOfLines = 0;
		self.label.textAlignment = NSTextAlignmentCenter;
		[self addSubview: self.label];
	}
	return self;
}

+ (void)makeToast:(NSString *)text
{
	[self makeToast:text duration:1];
}

+ (void)makeToast:(NSString *)text duration:(NSTimeInterval)duration
{
	UILabel * dummyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 80, 1000)];
	dummyLabel.font = [UIFont systemFontOfSize:18];
	dummyLabel.text = text;
	[dummyLabel sizeToFit];
	
	UIWindow* window = [UIApplication sharedApplication].delegate.window;
	
	CGRect frame = dummyLabel.frame;
	frame.size.width += 40;
	frame.size.height += 40;
	
	SDToast * toast = [[SDToast alloc] initWithFrame:frame];
	toast.label.text = text;
	toast.center = window.center;
	[window addSubview:toast];
	
	CATransition * transition = [[CATransition alloc] init];
	transition.type = kCATransitionFade;
	transition.duration = .1;
	[window.layer addAnimation:transition forKey:kCATransition];
	
	dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC);
	dispatch_after(time, dispatch_get_main_queue(), ^(void){
		[toast removeFromSuperview];

		CATransition * transition = [[CATransition alloc] init];
		transition.type = kCATransitionFade;
		transition.duration = .1;
		[window.layer addAnimation:transition forKey:kCATransition];
	});
}

@end
