//
//  UIImage+Resize.h
//  Corso12
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)

- (UIImage *)scaleToSize: (CGSize)size;
- (UIImage *)scaleToSize: (CGSize)size scale: (CGFloat)scale;
- (UIImage *)rotateImage: (CGFloat)radians;
- (UIImage *)centralSquareImage;
- (UIImage *)croppedImageInRect: (CGRect)rect;

@end
