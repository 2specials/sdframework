//
//  UIView+Screenshot.m
//  Flics
//
//  Created by Silvio Daminato on 17/07/13.
//  Copyright (c) 2013 Corso12 SRL. All rights reserved.
//

#import "UIView+Screenshot.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (Screenshot)

- (UIImage *)screenshot
{
	return [self screenshotWithScale:0.0f];
}

- (UIImage *)screenshotWithScale:(CGFloat)scale
{
	UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, scale);
	CGContextRef context = UIGraphicsGetCurrentContext();
	[self.layer renderInContext:context];
	UIImage * capturedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return capturedImage;
}

@end
