//
//  SDInfiniteScrollFooterView.m
//  Kenwood Club
//
//  Created by Silvio Daminato on 14/10/15.
//  Copyright © 2015 2Specials srl. All rights reserved.
//

#import "SDInfiniteScrollFooterView.h"

@implementation SDInfiniteScrollFooterView

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self addSubview:self.activityIndicator];
		self.activityIndicator.center = CGPointMake(frame.size.width / 2, frame.size.height / 2);
	}
	return self;
}

- (UIActivityIndicatorView *)activityIndicator
{
	if (!_activityIndicator) {
		_activityIndicator = [[UIActivityIndicatorView alloc] init];
		_activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
		_activityIndicator.hidesWhenStopped = YES;
		_activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
	}
	return _activityIndicator;
}

@end
