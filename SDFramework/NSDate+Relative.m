//
//  NSDate+Relative.m
//  SDFramework
//
//  Created by Silvio Daminato on 01/07/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import "NSDate+Relative.h"

@implementation NSDate (Relative)

- (NSString *)stringWithElapsedTimeSinceNow
{
	NSCalendar			*calendar;
	NSDateComponents	*dateComponents;
	NSMutableString		*string;
	
	calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
	dateComponents = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond)
								 fromDate:self
								   toDate:[NSDate date]
								  options: 0];
	
	string = [NSMutableString string];
	if(dateComponents.year) {
		if(dateComponents.year > 1)
			[string appendFormat: NSLocalizedString(@"%i years ago", @"Stringa che indica il tempo trascorso dalla data odierna in anni"), dateComponents.year];
		else
			[string appendFormat: NSLocalizedString(@"%i year ago", @"Stringa che indica il tempo trascorso dalla data odierna in anni (singolare)"), dateComponents.year];
	} else if(dateComponents.month) {
		if(dateComponents.month > 1)
			[string appendFormat: NSLocalizedString(@"%i months ago", @"Stringa che indica il tempo trascorso dalla data odierna in mesi"), dateComponents.month];
		else
			[string appendFormat: NSLocalizedString(@"%i month ago", @"Stringa che indica il tempo trascorso dalla data odierna in mesi (singolare)"), dateComponents.month];
	} else if(dateComponents.day) {
		if(dateComponents.day > 1)
			[string appendFormat: NSLocalizedString(@"%i days ago", @"Stringa che indica il tempo trascorso dalla data odierna in giorni"), dateComponents.day];
		else
			[string appendFormat: NSLocalizedString(@"%i day ago", @"Stringa che indica il tempo trascorso dalla data odierna in giorni (singolare)"), dateComponents.day];
	} else if(dateComponents.hour) {
		if(dateComponents.hour > 1)
			[string appendFormat: NSLocalizedString(@"%i hours ago", @"Stringa che indica il tempo trascorso dalla data odierna in ore"), dateComponents.hour];
		else
			[string appendFormat: NSLocalizedString(@"%i hour ago", @"Stringa che indica il tempo trascorso dalla data odierna in ore (singolare)"), dateComponents.hour];
	} else if(dateComponents.minute) {
		if(dateComponents.minute > 1)
			[string appendFormat: NSLocalizedString(@"%i minutes ago", @"Stringa che indica il tempo trascorso dalla data odierna in minuti"), dateComponents.minute];
		else
			[string appendFormat: NSLocalizedString(@"%i minute ago", @"Stringa che indica il tempo trascorso dalla data odierna in minuti (singolare)"), dateComponents.minute];
	} else if(dateComponents.second) {
		if(dateComponents.second > 1)
			[string appendFormat: NSLocalizedString(@"%i seconds ago", @"Stringa che indica il tempo trascorso dalla data odierna in secondi"), dateComponents.second];
		else
			[string appendFormat: NSLocalizedString(@"%i second ago", @"Stringa che indica il tempo trascorso dalla data odierna in secondi (singolare)"), dateComponents.second];
	} else {
		[string appendFormat: NSLocalizedString(@"Now", @""), 1];
	}
	
	return string;
}

- (NSString *)smartDateStringWithTime
{
	NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.timeStyle = NSDateFormatterShortStyle;
	
	NSMutableString * date = self.smartDateString.mutableCopy;
	[date appendFormat:@", %@", [dateFormatter stringFromDate:self]];
	return date;
}

- (NSString *)smartDateString
{
	NSDateFormatter * yesterdayDateFormatter = [[NSDateFormatter alloc] init];
	[yesterdayDateFormatter setDateFormat:@"yyyyMMdd"];
	
	// -- Oggi?
	
	if ([[yesterdayDateFormatter stringFromDate:[NSDate date]] isEqualToString:[yesterdayDateFormatter stringFromDate:self]]) {
		NSDateFormatter * timeDateFormatter = [[NSDateFormatter alloc] init];
		timeDateFormatter.timeStyle = NSDateFormatterShortStyle;
		return [timeDateFormatter stringFromDate:self];
//		return NSLocalizedString(@"Today", @"");
	}
	
	// -- Ieri?
	
	NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
	NSDate * midnight = [NSDate date];
	NSUInteger preservedComponents = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay);
	midnight = [calendar dateFromComponents:[calendar components:preservedComponents fromDate:midnight]];
	
	NSDateComponents * dateComponents = [calendar components:NSCalendarUnitMinute fromDate:self toDate:midnight options:0];
	if (dateComponents.minute < 0) {
		return NSLocalizedString(@"Future", @"");
	}
	if (dateComponents.minute <= 1440) {
		return NSLocalizedString(@"Yesterday", @"");
	}
	
	// -- Questa settimana?
	
	dateComponents = [calendar components:NSCalendarUnitDay fromDate:self toDate:midnight options:0];
	if (dateComponents.day < 6) {
		NSDateFormatter * dayDateFormatter = [[NSDateFormatter alloc] init];
		[dayDateFormatter setDateFormat:@"EEEE"];
		return [dayDateFormatter stringFromDate:self];
	}
	
	
	// -- Quest'anno?
	
	NSDateFormatter * thisYearDateFormatter = [[NSDateFormatter alloc] init];
	[thisYearDateFormatter setDateFormat:@"yyyy"];
	
	if ([[thisYearDateFormatter stringFromDate:[NSDate date]] isEqualToString:[thisYearDateFormatter stringFromDate:self]]) {
		NSDateFormatter * thisYearDateFormatter = [[NSDateFormatter alloc] init];
		[thisYearDateFormatter setDateFormat:@"EEE d MMMM"];
		
		return [[thisYearDateFormatter stringFromDate:self] mutableCopy];
	}
	
	// -- Data completa
	
	NSDateFormatter * defaultDateFormatter = [[NSDateFormatter alloc] init];
	defaultDateFormatter.dateStyle = NSDateFormatterLongStyle;
	
	return [[defaultDateFormatter stringFromDate:self] mutableCopy];
	
}

@end
