//
//  NSDate+Midnight.m
//  SDFramework
//
//  Created by Silvio Daminato on 22/05/17.
//  Copyright © 2017 2Specials SRL. All rights reserved.
//

#import "NSDate+Midnight.h"

@implementation NSDate (Midnight)

- (NSDate *)midnight
{
	NSCalendar * calendar = NSCalendar.currentCalendar;
	NSCalendarUnit preservedComponents = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay);
	NSDateComponents * components = [calendar components:preservedComponents fromDate:self];
	return [calendar dateFromComponents:components];
}

@end
