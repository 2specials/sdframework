//
//  SDImageView.h
//  SDFramework
//
//  Created by Silvio Daminato on 18/01/18.
//  Copyright © 2018 2Specials SRL. All rights reserved.
//

#import "SDRoundImageView.h"

@interface SDImageView : SDRoundImageView

@property (nonatomic) IBInspectable CGFloat cornerRadius;

@end
