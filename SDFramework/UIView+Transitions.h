//
//  UIView+Transitions.h
//  SDFramework
//
//  Created by Silvio Daminato on 18/01/17.
//  Copyright © 2017 2Specials SRL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Transitions)

- (void)addTransitionWithType:(NSString *)type subtype:(NSString *)subtype duration:(NSTimeInterval)duration;

@end
