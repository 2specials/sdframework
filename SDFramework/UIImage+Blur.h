//
//  UIImage+Blur.h
//  iSetlist
//
//  Created by Silvio Daminato on 20/08/14.
//  Copyright (c) 2014 Silvio Daminato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Blur)

- (UIImage *)blurredmage;

@end
