//
//  SDFlipSegue.h
//  SDFramework
//
//  Created by Silvio Daminato on 12/04/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDFlipSegue : UIStoryboardSegue

@end
