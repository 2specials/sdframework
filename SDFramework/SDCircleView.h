//
//  SDCircleView.h
//  SDFramework
//
//  Created by Silvio Daminato on 12/04/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import "SDBorderedView.h"

//IB_DESIGNABLE

__attribute__ ((deprecated))
@interface SDCircleView : SDBorderedView

@end
