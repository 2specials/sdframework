//
//  NSString+HTMLTags.h
//  Kenwood Club
//
//  Created by Silvio Daminato on 08/10/15.
//  Copyright © 2015 2Specials srl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HTMLTags)

- (NSString *)stringByStrippingHTML;

@end
