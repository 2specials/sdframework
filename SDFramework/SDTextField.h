//
//  SDTextField.h
//  SDFramework
//
//  Created by Silvio Daminato on 07/04/16.
//  Copyright © 2016 2Specials SRL. All rights reserved.
//

#import <UIKit/UIKit.h>

//IB_DESIGNABLE

@interface SDTextField : UITextField

@property (nonatomic, strong) IBInspectable UIColor * placeholderColor;
@property (nonatomic) IBInspectable BOOL doneButtonAccessoryInput;
@property (nonatomic, strong) IBInspectable UIColor * doneButtonTintColor;
@property (nonatomic, strong) IBInspectable UIColor * toolbarTintColor;

@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic, strong) IBInspectable UIColor * borderColor;
@property (nonatomic) IBInspectable CGFloat cornerRadius;

@end
