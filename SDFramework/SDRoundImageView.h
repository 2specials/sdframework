//
//  SDRoundImageView.h
//  BitMe
//
//  Created by Silvio Daminato on 10/08/15.
//  Copyright (c) 2015 Silvio Daminato. All rights reserved.
//

#import <UIKit/UIKit.h>

//IB_DESIGNABLE

@interface SDRoundImageView : UIImageView

@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic, strong) IBInspectable UIColor * borderColor;

@end
