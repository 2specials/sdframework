# Come importare SDFramework in un nuovo progetto #

Trascinare il file SDFramework.framework nel progetto Xcode

## Progetto Objective-C ##

Aggiungere in Build settings i Framework search paths:

- `${HOME}/Documents/SDFramework`

- `${inherited}`

Aggiungere i pod

- `pod 'AFNetworking', '~> 3.0'`

- `pod 'Reachability'`

## Progetto Swift ##

Aggiungere in Build settings i Framework search paths:

- `${HOME}/Documents/SDFramework`

- `${inherited}`

Aggiungere i pod

- `pod 'AFNetworking', '~> 3.0'`

- `pod 'Reachability'`

Creare bridging header importando l'umbrella header

- `#import <SDFramework/SDFramework.h>`

Aggiungere tra Other Linker Flags:

- `-all_load`

# Uso di SDNetworkingManager #

Aggiungere a Info.plist:

	<key>SDApiPrefix</key>
	<string>/</string>
	<key>SDAuthorizationTokenHeaderKey</key>
	<string>Authorization-Token</string>
	<key>SDHostName</key>
	<string>ec2-35-157-171-161.eu-central-1.compute.amazonaws.com</string>
	<key>SDUseHttps</key>
	<false/>